
// necorrtask1Dlg.h : ���� ���������
//

#pragma once


// ���������� ���� Cnecorrtask1Dlg
class Cnecorrtask1Dlg : public CDialogEx
{
// ��������
public:
	Cnecorrtask1Dlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_NECORRTASK1_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	//������� ���������
	CWnd * PicWnd;
	CDC * PicDc;
	CRect Pic;

	CWnd * PicWndImp;
	CDC * PicDcImp;
	CRect PicImp;

	CWnd * PicWndSvert;
	CDC * PicDcSvert;
	CRect PicSvert;

	double Min, Max, Min1, Max1, Min2, Max2;
	double xx0, xxmax, yy0, yymax, xxi, yyi;

	//���������� �����
	CPen osi_pen;		// ����� ��� ����
	CPen setka_pen;		// ��� �����
	CPen signal_pen;		// ��� ������� �������
	CPen signal2_pen;

public:
	//CButton StartStopOptimization;
	CButton StartStop;
	BOOL bRunTh = false;		// ����������, ������������, ������� �� �������
	CString start = L"������";		// ������ � ������ ������ ��� ������� ������
	CString stop = L"���������";

	double A1_sign;
	double A2_sign;
	double A3_sign;
	double A1_imp;
	double A2_imp;
	double dis1_sign;
	double dis2_sign;
	double dis3_sign;
	double t01_sign;
	double t02_sign;
	double t03_sign;
	double dis1_imp;
	double dis2_imp;
	double t01_imp;
	double t02_imp;
	int t_sign;
	CEdit error;							//���������� ����� ���������
	CEdit znuchFunc;
	int percent_shum;						//��� � ���������

	double* Signal = new double[t_sign];		//���������� ������ ��� �������
	double* svertka = new double[t_sign];			//���������� ������ ��� �������
	double* h_norm = new double[t_sign];			//���������� ������ ��� ������������� ���.��������������
	double* lyambda = new double[t_sign];			//���������� ������ �����(�����.��-�� ��������)
	double* VosstSignal = new double[t_sign];

	afx_msg double signal(int t);			//������� �������
	afx_msg double MHJ(int kk, double* x);	//����� ����-������
	afx_msg double function(double* x);		//����������
	afx_msg void PererisovkaDC(double* Mass, CDC* WinDc, CRect WinPic, CPen* graphpen, double AbsMax);			//������� ����������� DC
	afx_msg void PererisovkaImp(double* Mass1, CPen* graph1pen, double* Mass2, CPen* graph2pen, CDC* WinDc, CRect WinPic, double AbsMax);			//������� ����������� �������
	afx_msg void Mashtab(double arr[], int dim, double* mmin, double* mmax);
	afx_msg double Psi();					//������������ ��� ����
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);

	afx_msg void OnBnClickedIshdann();
	afx_msg void OnBnClickedVosst();
	afx_msg void OnBnClickedCancel();

	bool SignalFlag, ImpulseFlag, SvertkaFlag, DeconvFlag;
	char err[1000];
	char func[1000];
	char znach[1000];
	double otkl;

	DWORD dwThread;
	HANDLE hThread;
};
