
// necorrtask1Dlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "necorrtask1.h"
#include "necorrtask1Dlg.h"
#include "afxdialogex.h"

#include <iostream>
#include <math.h>
#include <time.h>
#include <random>
#include <fstream>
#include <conio.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;


// ���������� ���� Cnecorrtask1Dlg



Cnecorrtask1Dlg::Cnecorrtask1Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cnecorrtask1Dlg::IDD, pParent)
	, A1_sign(5)
	, A2_sign(2)
	, A3_sign(3.5)
	, A1_imp(1)
	, A2_imp(1)
	, dis1_sign(4)
	, dis2_sign(3)
	, dis3_sign(5)
	, t01_sign(6)
	, t02_sign(30)
	, t03_sign(42)
	, dis1_imp(5)
	, dis2_imp(5)
	, t01_imp(0)
	, t02_imp(50)
	, t_sign(50)
	, percent_shum(0)
	, SignalFlag(false)
	, ImpulseFlag(false)
	, SvertkaFlag(false)
	, DeconvFlag(false)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cnecorrtask1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, A1_sign);
	DDX_Text(pDX, IDC_EDIT2, A2_sign);
	DDX_Text(pDX, IDC_EDIT3, A3_sign);
	DDX_Text(pDX, IDC_EDIT10, A1_imp);
	DDX_Text(pDX, IDC_EDIT11, A2_imp);
	DDX_Text(pDX, IDC_EDIT4, dis1_sign);
	DDX_Text(pDX, IDC_EDIT5, dis2_sign);
	DDX_Text(pDX, IDC_EDIT6, dis3_sign);
	DDX_Text(pDX, IDC_EDIT7, t01_sign);
	DDX_Text(pDX, IDC_EDIT8, t02_sign);
	DDX_Text(pDX, IDC_EDIT9, t03_sign);
	DDX_Text(pDX, IDC_EDIT13, dis1_imp);
	DDX_Text(pDX, IDC_EDIT14, dis2_imp);
	DDX_Text(pDX, IDC_EDIT16, t01_imp);
	DDX_Text(pDX, IDC_EDIT17, t02_imp);
	DDX_Text(pDX, IDC_EDIT19, t_sign);
	DDX_Control(pDX, IDC_STATIC_ERR, error);
	DDX_Text(pDX, IDC_EDIT20, percent_shum);
	DDX_Control(pDX, IDC_STATIC_ERR2, znuchFunc);
	DDX_Control(pDX, IDC_VOSST, StartStop);
}

BEGIN_MESSAGE_MAP(Cnecorrtask1Dlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_ISHDANN, &Cnecorrtask1Dlg::OnBnClickedIshdann)
	ON_BN_CLICKED(IDC_VOSST, &Cnecorrtask1Dlg::OnBnClickedVosst)
	ON_BN_CLICKED(IDC_CANCEL, &Cnecorrtask1Dlg::OnBnClickedCancel)
	ON_WM_TIMER()
	ON_WM_SYSCOMMAND()
END_MESSAGE_MAP()


// ����������� ��������� Cnecorrtask1Dlg

BOOL Cnecorrtask1Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������
	//���  ��������
	PicWnd = GetDlgItem(IDC_SIGNAL);
	PicDc = PicWnd->GetDC();
	PicWnd->GetClientRect(&Pic);

	PicWndImp = GetDlgItem(IDC_IMPULS);
	PicDcImp = PicWndImp->GetDC();
	PicWndImp->GetClientRect(&PicImp);

	PicWndSvert = GetDlgItem(IDC_SVERTKA);
	PicDcSvert = PicWndSvert->GetDC();
	PicWndSvert->GetClientRect(&PicSvert);

	// �����
	setka_pen.CreatePen(		//��� �����
		PS_DOT,					//����������
		1,						//������� 1 �������
		RGB(0, 0, 0));			//����  ������

	osi_pen.CreatePen(			//������������ ���
		PS_SOLID,				//�������� �����
		3,						//������� 3 �������
		RGB(0, 0, 0));			//���� ������

	signal_pen.CreatePen(			//������ ��������� �������
		PS_SOLID,				//�������� �����
		2,						//������� 2 �������
		RGB(0, 0, 255));			//���� �����

	signal2_pen.CreatePen(			//������ ���������������� �������
		PS_SOLID,				//�������� �����
		2,						//������� 2 �������
		RGB(255, 0, 0));		//���� red


	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void Cnecorrtask1Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		UpdateData(FALSE);

		if (SignalFlag == true)
		{
			PererisovkaDC(Signal, PicDc, Pic, &signal_pen, t_sign);
		}
		if (ImpulseFlag == true)
		{
			PererisovkaDC(h_norm, PicDcImp, PicImp, &signal_pen, t_sign);
		}
		if (SvertkaFlag == true)
		{
			PererisovkaDC(svertka, PicDcSvert, PicSvert, &signal_pen, t_sign);
		}
		if (DeconvFlag == true)
		{
			PererisovkaImp(Signal, &signal_pen, VosstSignal, &signal2_pen, PicDc, Pic, t_sign);
		}
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR Cnecorrtask1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void Cnecorrtask1Dlg::Mashtab(double arr[], int dim, double* mmin, double* mmax)		//���������� ������� ���������������
{
	*mmin = *mmax = arr[0];

	for (int i = 0; i < dim; i++)
	{
		if (*mmin > arr[i]) *mmin = arr[i];
		if (*mmax < arr[i]) *mmax = arr[i];
	}
}

DWORD WINAPI MyProc(PVOID pv)
{
	Cnecorrtask1Dlg* p = (Cnecorrtask1Dlg*)pv;
	p->MHJ(p->t_sign, p->lyambda);
	return 0;
}

void Cnecorrtask1Dlg::PererisovkaDC(double* Mass, CDC* WinDc, CRect WinPic, CPen* graphpen, double AbsMax)
{
	Mashtab(Mass, AbsMax, &Min, &Max);
	// ���������
	// �������� ��������� ����������
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(WinDc, WinPic.Width(), WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// ������� ���� ������� ����� ������
	MemDc->FillSolidRect(WinPic, RGB(255, 255, 255));

	// ��������� ����� ���������
	MemDc->SelectObject(&setka_pen);

	// ������������ ����� ����� ���������
	for (double i = WinPic.Width() / 6; i < WinPic.Width(); i += WinPic.Width() / 6)
	{
		MemDc->MoveTo(i, 0);
		MemDc->LineTo(i, WinPic.Height());
	}

	// �������������� ����� ����� ���������
	for (double i = WinPic.Height() / 4; i < WinPic.Height(); i += WinPic.Height() / 4)
	{
		MemDc->MoveTo(0, i);
		MemDc->LineTo(WinPic.Width(), i);
	}

	// ��������� ����
	MemDc->SelectObject(&osi_pen);

	// ��������� ��� X
	MemDc->MoveTo(0, WinPic.Height() * 9 / 10); 
	MemDc->LineTo(WinPic.Width(), WinPic.Height() * 9 / 10);
	
	// ��������� ��� Y
	MemDc->MoveTo(WinPic.Width() * 1 / 15, WinPic.Height());
	MemDc->LineTo(WinPic.Width() * 1 / 15, 0);

	// ��������� ����������� ���� ������
	MemDc->SetBkMode(TRANSPARENT);

	// ��������� ������
	CFont font;
	font.CreateFontW(14.5, 0, 0, 0, FW_REGULAR, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Century Gothic"));
	MemDc->SelectObject(&font);

	// ������� ��� X
	MemDc->TextOut(WinPic.Width() * 14 / 15 + 4, WinPic.Height() * 9 / 10 + 2, CString("t"));

	// ������� ��� Y
	MemDc->TextOut(WinPic.Width() * 1 / 15 + 10, 0, CString("A"));

	// ����� ������� ��� ���������
	xx0 = WinPic.Width() * 1 / 15; xxmax = WinPic.Width();
	yy0 = WinPic.Height() / 10; yymax = WinPic.Height() * 9 / 10;

	// ��������� ������� �������
	MemDc->SelectObject(graphpen);
	MemDc->MoveTo(xx0, yymax + (Mass[0] - Min) / (Max - Min) * (yy0 - yymax));

	for (int i = 0; i < t_sign; i++)
	{
		xxi = xx0 + (xxmax - xx0) * i / (t_sign - 1);
		yyi = yymax + (Mass[i] - Min) / (Max - Min) * (yy0 - yymax);
		MemDc->LineTo(xxi, yyi);
	}

	//����� �������� �������� �� ��� X
	MemDc->SelectObject(&font);
	for (int i = 1; i < 6; i++)
	{
		sprintf_s(znach, "%.1f", i * AbsMax / 6);
		MemDc->TextOut(i * WinPic.Width() / 6, WinPic.Height() * 9 / 10 + 2, CString(znach));
	}
	// �� ��� Y
	for (int i = 1; i < 5; i++)
	{
		sprintf_s(znach, "%.2f", Min + i * (Max - Min) / 4);
		MemDc->TextOut(0, WinPic.Height() * (9 - 2 * i) / 10, CString(znach));
	}

	// ����� �� �����
	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void Cnecorrtask1Dlg::PererisovkaImp(double* Mass1, CPen* graph1pen, double* Mass2, CPen* graph2pen, CDC* WinDc, CRect WinPic, double AbsMax)
{
	// ����� ������������� � ������������ ��������
	Mashtab(Mass1, AbsMax, &Min1, &Max1);
	Mashtab(Mass2, AbsMax, &Min2, &Max2);
	if (Max2 > Max1)
	{
		Max = Max2;
	}
	else
	{
		Max = Max1;
	}
	if (Min2 < Min1)
	{
		Min = Min2;
	}
	else
	{
		Min = Min1;
	}

	// ���������
	// �������� ��������� ����������
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(WinDc, WinPic.Width(), WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// ������� ���� ������� ����� ������
	MemDc->FillSolidRect(WinPic, RGB(255, 255, 255));

	// ��������� ����� ���������
	MemDc->SelectObject(&setka_pen);

	// ������������ ����� ����� ���������
	for (double i = 0; i < WinPic.Width(); i += WinPic.Width() / 6)
	{
		MemDc->MoveTo(i, 0);
		MemDc->LineTo(i, WinPic.Height());
	}

	// �������������� ����� ����� ���������
	for (double i = WinPic.Height() / 10; i < WinPic.Height(); i += WinPic.Height() / 5)
	{
		MemDc->MoveTo(0, i);
		MemDc->LineTo(WinPic.Width(), i);
	}

	// ��������� ����
	MemDc->SelectObject(&osi_pen);

	// ��������� ��� X
	MemDc->MoveTo(0, WinPic.Height() * 9 / 10); 
	MemDc->LineTo(WinPic.Width(), WinPic.Height() * 9 / 10);

	// ��������� ��� Y
	MemDc->MoveTo(WinPic.Width() * 1 / 15, WinPic.Height()); 
	MemDc->LineTo(WinPic.Width() * 1 / 15, 0);

	// ��������� ����������� ���� ������
	MemDc->SetBkMode(TRANSPARENT);

	// ��������� ������
	CFont font;
	font.CreateFontW(14.5, 0, 0, 0, FW_REGULAR, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Century Gothic"));
	MemDc->SelectObject(&font);

	// ������� ��� X
	MemDc->TextOut(WinPic.Width() * 14 / 15 + 4, WinPic.Height() * 9 / 10 + 2, CString("t"));

	// ������� ��� Y
	MemDc->TextOut(WinPic.Width() * 1 / 15 + 10, 0, CString("A"));

	// ����� ������� ��� ���������
	xx0 = WinPic.Width() * 1 / 15; xxmax = WinPic.Width();
	yy0 = WinPic.Height() / 10; yymax = WinPic.Height() * 9 / 10;

	// ��������� ������� �������
	MemDc->SelectObject(graph1pen);
	MemDc->MoveTo(xx0, yymax + (Mass1[0] - Min) / (Max - Min) * (yy0 - yymax));

	for (int i = 0; i < t_sign; i++)
	{
		xxi = xx0 + (xxmax - xx0) * i / (t_sign - 1);
		yyi = yymax + (Mass1[i] - Min) / (Max - Min) * (yy0 - yymax);
		MemDc->LineTo(xxi, yyi);
	}

	// ��������� ������� �������
	MemDc->SelectObject(graph2pen);
	MemDc->MoveTo(xx0, yymax + (Mass2[0] - Min) / (Max - Min) * (yy0 - yymax));

	for (int i = 0; i < t_sign; i++)
	{
		xxi = xx0 + (xxmax - xx0) * i / (t_sign - 1);
		yyi = yymax + (Mass2[i] - Min) / (Max - Min) * (yy0 - yymax);
		MemDc->LineTo(xxi, yyi);
	}

	// ����� �������� �������� �� ��� X
	MemDc->SelectObject(&font);
	for (int i = 1; i < 6; i++)
	{
		sprintf_s(znach, "%.1f", i * AbsMax / 6);
		MemDc->TextOut(i * (WinPic.Width() / 6) - AbsMax * 0.09, WinPic.Height() * 9 / 10 + 2, CString(znach));
	}

	// �� ��� Y
	for (int i = 1; i < 5; i++)
	{
		sprintf_s(znach, "%.2f", Min + i * (Max - Min) / 4);
		MemDc->TextOut(0 + AbsMax * 0.1, WinPic.Height() * (9 - 2 * i) / 10, CString(znach));
	}

	// ����� �� �����
	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);

	delete MemDc;
}

double Cnecorrtask1Dlg::signal(int t)			//������� ��� �������� �������
{
	double A[] = {A1_sign, A2_sign, A3_sign};
	double t0[] = {t01_sign, t02_sign, t03_sign};
	double del[] = {dis1_sign, dis2_sign, dis3_sign};
	double s = 0;

	for (int j = 0; j <= 2; j++)
	{
		s += A[j] * exp(-(((t - t0[j]) / del[j]) * ((t - t0[j]) / del[j])));
	}

	return s;
}

double Cnecorrtask1Dlg::Psi()		//������������ ��� ����
{
	float r = 0;
	for (int i = 1; i <= 12; i++)
	{
		r += ((rand() % 100) / (100 * 1.0) * 2) - 1;		// [-1;1]
	}
	return r / 12;
}

double Cnecorrtask1Dlg::function(double* x)			//���������� ��� ���
{
	// ��������� �������������� �������
	// ���������� �������� �������
	// ���������� ���������� �������� ������ ������, � ��������� ������ �������� ��������� �������

	double* sum = new double[t_sign];
	memset(sum, 0, t_sign * sizeof(double));

	for (int i = 0; i < t_sign; i++)
	{
		for (int j = 0; j < t_sign; j++)
		{
			if (i - j < 0)
				sum[i] += x[j] * h_norm[t_sign + (i - j)];
			else
				sum[i] += x[j] * h_norm[i - j];
		}
		VosstSignal[i] = exp(-1 - sum[i]);
	}

	double Error = 0.;

	double* yi = new double[t_sign];
	memset(yi, 0, t_sign * sizeof(double));

	for (int i = 0; i < t_sign; i++)
	{
		for (int j = 0; j < t_sign; j++)
		{
			if (i - j < 0)
				yi[i] += VosstSignal[j] * h_norm[t_sign + (i - j)];
			else
				yi[i] += VosstSignal[j] * h_norm[i - j];
		}
		Error += (svertka[i] - yi[i]) * (svertka[i] - yi[i]);
	}

	return Error;

	delete yi;
	delete sum;
}

double Cnecorrtask1Dlg::MHJ(int kk, double* x)
{
	// kk - ���������� ����������; x - ������ ����������
	double  TAU = 1.e-6; // �������� ���������� 
	int i, j, bs, ps;
	double z, h, k, fi, fb;
	double* b = new double[kk];
	double* y = new double[kk];
	double* p = new double[kk];
	h = 1;
	x[0] = 1;
	for (i = 1; i < kk; i++)
	{
		x[i] = (double)rand() / RAND_MAX; // �������� ��������� �����������
	}

	k = h;
	for (i = 0; i < kk; i++)
	{
		y[i] = p[i] = b[i] = x[i];
	}
	fi = function(x);
	ps = 0;
	bs = 1;
	fb = fi;
	j = 0;
	while (1)
	{
		x[j] = y[j] + k;
		z = function(x);

		if (z >= fi)
		{
			x[j] = y[j] - k;
			z = function(x);
			if (z < fi)
			{
				y[j] = x[j];
			}
			else  x[j] = y[j];
		}
		else  y[j] = x[j];
		fi = function(x);
		if (j < kk - 1)
		{
			j++;
			continue;
		}

		if (fi + 1e-8 >= fb)
		{
			if (ps == 1 && bs == 0)
			{
				for (i = 0; i < kk; i++)
				{
					p[i] = y[i] = x[i] = b[i];
				}
				z = function(x);
				bs = 1;
				ps = 0;
				fi = z;
				fb = z;
				j = 0;
				continue;
			}
			k /= 10.;
			if (k < TAU)
			{
				break;
			}
			j = 0;
			continue;
		}
		for (i = 0; i < kk; i++)
		{
			p[i] = 2 * y[i] - b[i];
			b[i] = y[i];
			x[i] = p[i];
			y[i] = x[i];
		}
		z = function(x);
		fb = fi;
		ps = 1;
		bs = 0;
		fi = z;
		j = 0;


		sprintf_s(func, "%.8f", fb/t_sign);
		znuchFunc.SetWindowTextW((CString)func);

		otkl = 0;
		for (int i = 0; i < t_sign; i++)
		{
			otkl += (Signal[i] - VosstSignal[i]) * (Signal[i] - VosstSignal[i]);
		}

		sprintf_s(err, "%.8f", otkl / t_sign);
		error.SetWindowTextW((CString)err);

		Invalidate(0);

		Sleep(30);
	} //  end of while(1)
	for (i = 0; i < kk; i++)
	{
		x[i] = p[i];
	}
	delete b;
	delete y;
	delete p;

	return fb;
}

void Cnecorrtask1Dlg::OnBnClickedIshdann()
{
	// TODO: �������� ���� ��� ����������� �����������
	UpdateData(TRUE);
	SignalFlag = true;
	ImpulseFlag = true;
	SvertkaFlag = true;

	double* ish_sign = new double[t_sign];
	double* h_sign = new double[t_sign];

	double norm = 0.;

	for (int i = 0; i < t_sign; i++)
	{
		ish_sign[i] = signal(i);	//������� �������� ������
		h_sign[i] = A1_imp * exp(-(((i - t01_imp) / dis1_imp) * ((i - t01_imp) / dis1_imp))) +
			A2_imp * exp(-(((i - t02_imp) / dis2_imp) * ((i - t02_imp) / dis2_imp)));
		norm += h_sign[i];
	}

	for (int i = 0; i < t_sign; i++)
	{
		h_norm[i] = h_sign[i] / norm;
		Signal[i] = ish_sign[i];
	}

	memset(svertka, 0, t_sign * sizeof(double));
	for (int i = 0; i < t_sign; i++)
	{
		for (int j = 0; j < t_sign; j++)
		{
			if (i - j < 0)
				svertka[i] += ish_sign[j] * h_norm[t_sign + (i - j)];
			else
				svertka[i] += ish_sign[j] * h_norm[i - j];
		}
	}

	//��������� ����

	double d = percent_shum / 100.;

	double energ_signal = 0;
	for (int t = 0; t < t_sign; t++)
	{
		energ_signal += svertka[t] * svertka[t];
	}

	double* psi = new double[t_sign];
	memset(psi, 0, t_sign * sizeof(double));

	for (int t = 0; t < t_sign; t++)
	{
		psi[t] = Psi();
	}

	double qpsi = 0;
	for (int t = 0; t < t_sign; t++)
	{
		qpsi += psi[t] * psi[t];
	}

	double alpha = sqrt(d * energ_signal / qpsi);
	
	double* Shum = new double[t_sign];
	memset(Shum, 0, t_sign * sizeof(double));

	for (int i = 0; i < t_sign; i++)
	{
		Shum[i] = svertka[i] + alpha * psi[i];
	}

	for (int i = 0; i < t_sign; i++)
	{
		svertka[i] = Shum[i];
	}

	PererisovkaDC(Signal, PicDc, Pic, &signal_pen, t_sign);
	PererisovkaDC(h_norm, PicDcImp, PicImp, &signal_pen, t_sign);
	PererisovkaDC(svertka, PicDcSvert, PicSvert, &signal_pen, t_sign);

	delete[] ish_sign;
	delete[] h_sign;
	delete[] psi;
	delete[] Shum;

	TerminateThread(hThread, 0);		//������� �����
	CloseHandle(hThread);
	hThread = NULL;

	StartStop.SetWindowTextW(start);
	bRunTh = false;

	DeconvFlag = false;
	PererisovkaDC(Signal, PicDc, Pic, &signal_pen, t_sign);
	PererisovkaDC(h_norm, PicDcImp, PicImp, &signal_pen, t_sign);
	PererisovkaDC(svertka, PicDcSvert, PicSvert, &signal_pen, t_sign);

	error.SetWindowTextW(L"");
	znuchFunc.SetWindowTextW(L"");
	//znuchFunc = " ";
	//error = " ";
}


void Cnecorrtask1Dlg::OnBnClickedVosst()
{
	// TODO: �������� ���� ��� ����������� �����������
	UpdateData(TRUE);
	SignalFlag = false;
	ImpulseFlag = false;
	SvertkaFlag = false;

	if (!bRunTh)
	{
		StartStop.SetWindowTextW(stop);
		if (hThread == NULL)
		{
			for (int i = 0; i < t_sign; i++)
			{
				lyambda[i] = 0;
				VosstSignal[i] = 0;
			}

			hThread = CreateThread(NULL, 0, MyProc, this, 0, &dwThread);
			DeconvFlag = true;
		}
		else
		{
			ResumeThread(hThread);
		}
		bRunTh = true;
	}
	else
	{
		StartStop.SetWindowTextW(start);
		bRunTh = false;

		SuspendThread(hThread);
	}
}

void Cnecorrtask1Dlg::OnBnClickedCancel()
{
	// TODO: �������� ���� ��� ����������� �����������
	CDialogEx::OnCancel();
}

void Cnecorrtask1Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: �������� ���� ��� ����������� ��������� ��� ����� ������������

	CDialogEx::OnSysCommand(nID, lParam);
}
